package AIF;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

import AIF.Missions.*;
import org.junit.BeforeClass;
import org.junit.Test;


//      Partially completed !!!
public class F15SetMissionFunctionTest {
    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass(){
        aifUtil = new AIFUtil();
    }




    @Test//Unexecutable Mission -> BdaMission
    public void testF15UnexecutableMission() throws MissionTypeException{
        try{
            aifUtil.getAerialVehiclesHashMap().get("F15").setMission(aifUtil.getAllMissions().get("bda"));
            fail();
        }catch (MissionTypeException missionTypeException){
            assertTrue(true);
        }
    }
}
